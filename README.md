# Views Argument Order Sort

Using this module will give you a new sort type with a variety of options where you can choose what field to apply the argument values to for sorting.

- For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_arg_order_sort).

- Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_arg_order_sort).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Usage

1. Use composer require `'drupal/views_arg_order_sort'`.
2. Create a view and add contextual filter e.g for Content ID.
3. Add a basic view sort field 'Multi-item Argument Order'.
4. Select 'Content ID' from the select list and configure other settings as you want.
5. Pass your ids to the view filter using custom code.
6. You can see nodes in view with your desired order.


## Configuration

There is no configuration.


## Maintainers

- Szczepan Musial (lamp5) - [lamp5](https://www.drupal.org/u/lamp5)
- Michael Cooper (soyarma) - [soyarma](https://www.drupal.org/u/soyarma)
